module.exports = {
  root: true,
  env: {
    node: true,
    es6: true,
    commonjs: true,
    browser: true
  },
  extends: [
    // 'vue',
    // 'plugin:vue/recommended',
    'plugin:vue/essential',
    '@vue/standard'
  ],
  plugins: [
    "vue"
  ],
  parserOptions: {
    parser: 'babel-eslint'
  },
  'ecmaFeatures': {
    'arrowFunctions': true,
    'destructuring': true,
    'classes': true,
    'defaultParams': true,
    'blockBindings': true,
    'modules': true,
    'objectLiteralComputedProperties': true,
  },
  rules: {
    'indent': ['error', 2],
    'no-tabs': 'off'
  }
}
