module.exports = {
  plugins:{
    "postcss-import":{},
    "postcss-cssnext":{},
    'postcss-px-to-viewport':{
      viewportWidth:750,
      viewportHeight:1334,
      unitPrecision:3,
      viewportUnit:'vw',
      selectorBlackList:['.igonre','.hairlines','van'],
      minPixelValue:1,
      mediaQuery:false
    }
  }
}