export default {
  data1: [{
    date: '2019-10',
    type: '金额',
    value: 260,
    color: '#6195FF'
  }, {
    date: '2019-11',
    type: '金额',
    value: 980,
    color: '#6195FF'
  }, {
    date: '2019-12',
    type: '金额',
    value: 600,
    color: '#6195FF'
  }, {
    date: '2020-01',
    type: '金额',
    value: 1800,
    color: '#6195FF'
  }, {
    date: '2020-02',
    type: '金额',
    value: 1400,
    color: '#6195FF'
  }],
  data2: [{
    date: '2019-10',
    type: '逾期率',
    value: 0,
    color: '#6195FF'
  }, {
    date: '2019-11',
    type: '逾期率',
    value: 0,
    color: '#6195FF'
  }, {
    date: '2019-12',
    type: '逾期率',
    value: 0,
    color: '#6195FF'
  }, {
    date: '2020-01',
    type: '逾期率',
    value: 0,
    color: '#6195FF'
  }, {
    date: '2020-02',
    type: '逾期率',
    value: 0,
    color: '#6195FF'
  }, {
    date: '2020-03',
    type: '逾期率',
    value: 0,
    color: '#6195FF'
  }],
  data3: [
    {
      date: '2019-10',
      type: '行业TOP5平均',
      value: 40,
      color: '#F9BB4C'
    }, {
      date: '2019-11',
      type: '行业TOP5平均',
      value: 290,
      color: '#F9BB4C'
    }, {
      date: '2019-12',
      type: '行业TOP5平均',
      value: 230,
      color: '#F9BB4C'
    }, {
      date: '2020-01',
      type: '行业TOP5平均',
      value: 490,
      color: '#F9BB4C'
    }, {
      date: '2020-02',
      type: '行业TOP5平均',
      value: 520,
      color: '#F9BB4C'
    }, {
      date: '2020-03',
      type: '行业TOP5平均',
      value: 880,
      color: '#F9BB4C'
    },

    {
      date: '2019-10',
      type: '当前累计贷款规模',
      value: 90,
      color: '#8759E5'
    }, {
      date: '2019-11',
      type: '当前累计贷款规模',
      value: 390,
      color: '#8759E5'
    }, {
      date: '2019-12',
      type: '当前累计贷款规模',
      value: 330,
      color: '#8759E5'
    }, {
      date: '2020-01',
      type: '当前累计贷款规模',
      value: 590,
      color: '#8759E5'
    }, {
      date: '2020-02',
      type: '当前累计贷款规模',
      value: 620,
      color: '#8759E5'
    }, {
      date: '2020-03',
      type: '当前累计贷款规模',
      value: 1229.23,
      color: '#8759E5'
    },

    {
      date: '2019-10',
      type: '战区平均',
      value: 20,
      color: '#6195FF'
    }, {
      date: '2019-11',
      type: '战区平均',
      value: 220,
      color: '#6195FF'
    }, {
      date: '2019-12',
      type: '战区平均',
      value: 130,
      color: '#6195FF'
    }, {
      date: '2020-01',
      type: '战区平均',
      value: 390,
      color: '#6195FF'
    }, {
      date: '2020-02',
      type: '战区平均',
      value: 420,
      color: '#6195FF'
    }, {
      date: '2020-03',
      type: '战区平均',
      value: 600,
      color: '#6195FF'
    }

  ],
  data4: [{
    date: '2019-10',
    type: '行业TOP5平均',
    value: 5.5,
    color: '#6195FF'
  }, {
    date: '2019-11',
    type: '行业TOP5平均',
    value: 4.3,
    color: '#6195FF'
  }, {
    date: '2019-12',
    type: '行业TOP5平均',
    value: 3.1,
    color: '#6195FF'
  }, {
    date: '2020-01',
    type: '行业TOP5平均',
    value: 4.2,
    color: '#6195FF'
  }, {
    date: '2020-02',
    type: '行业TOP5平均',
    value: 5.4,
    color: '#6195FF'
  }, {
    date: '2020-03',
    type: '行业TOP5平均',
    value: 3.7,
    color: '#6195FF'
  },
  {
    date: '2019-10',
    type: '贷款收益率',
    value: 6.0,
    color: '#F9BB4C'
  }, {
    date: '2019-11',
    type: '贷款收益率',
    value: 4.1,
    color: '#F9BB4C'
  }, {
    date: '2019-12',
    type: '贷款收益率',
    value: 4.5,
    color: '#F9BB4C'
  }, {
    date: '2020-01',
    type: '贷款收益率',
    value: 4.2,
    color: '#F9BB4C'
  }, {
    date: '2020-02',
    type: '贷款收益率',
    value: 6.0,
    color: '#F9BB4C'
  }, {
    date: '2020-03',
    type: '贷款收益率',
    value: 4.0,
    color: '#F9BB4C'
  },

  {
    date: '2019-10',
    type: '战区平均',
    value: 4.0,
    color: '#8759E5'
  }, {
    date: '2019-11',
    type: '战区平均',
    value: 3.0,
    color: '#8759E5'
  }, {
    date: '2019-12',
    type: '战区平均',
    value: 2.9,
    color: '#8759E5'
  }, {
    date: '2020-01',
    type: '战区平均',
    value: 3.2,
    color: '#8759E5'
  }, {
    date: '2020-02',
    type: '战区平均',
    value: 2.8,
    color: '#8759E5'
  }, {
    date: '2020-03',
    type: '战区平均',
    value: 2.1,
    color: '#8759E5'
  }
  ],
  data6: [{
    date: '2019-10',
    type: '行业TOP5平均',
    value: 55,
    color: '#6195FF'
  }, {
    date: '2019-11',
    type: '行业TOP5平均',
    value: 80,
    color: '#6195FF'
  }, {
    date: '2019-12',
    type: '行业TOP5平均',
    value: 70,
    color: '#6195FF'
  }, {
    date: '2020-01',
    type: '行业TOP5平均',
    value: 75,
    color: '#6195FF'
  }, {
    date: '2020-02',
    type: '行业TOP5平均',
    value: 85,
    color: '#6195FF'
  }, {
    date: '2020-03',
    type: '行业TOP5平均',
    value: 105,
    color: '#6195FF'
  },

  {
    date: '2019-10',
    type: '当前累计贷款规模',
    value: 87,
    color: '#F9BB4C'
  }, {
    date: '2019-11',
    type: '当前累计贷款规模',
    value: 97,
    color: '#F9BB4C'
  }, {
    date: '2019-12',
    type: '当前累计贷款规模',
    value: 107,
    color: '#F9BB4C'
  }, {
    date: '2020-01',
    type: '当前累计贷款规模',
    value: 127,
    color: '#F9BB4C'
  }, {
    date: '2020-02',
    type: '当前累计贷款规模',
    value: 137,
    color: '#F9BB4C'
  }, {
    date: '2020-03',
    type: '当前累计贷款规模',
    value: 167,
    color: '#F9BB4C'
  },

  {
    date: '2019-10',
    type: '战区平均',
    value: 100,
    color: '#8759E5'
  }, {
    date: '2019-11',
    type: '战区平均',
    value: 120,
    color: '#8759E5'
  }, {
    date: '2019-12',
    type: '战区平均',
    value: 120,
    color: '#8759E5'
  }, {
    date: '2020-01',
    type: '战区平均',
    value: 150,
    color: '#8759E5'
  }, {
    date: '2020-02',
    type: '战区平均',
    value: 160,
    color: '#8759E5'
  }, {
    date: '2020-03',
    type: '战区平均',
    value: 175,
    color: '#8759E5'
  }
  ],
  data7: [{
    date: '2019-10',
    type: '行业TOP5平均',
    value: 5.2,
    color: '#6195FF',
    unit: '%'
  }, {
    date: '2019-11',
    type: '行业TOP5平均',
    value: 4.3,
    color: '#6195FF'
  }, {
    date: '2019-12',
    type: '行业TOP5平均',
    value: 3.1,
    color: '#6195FF'
  }, {
    date: '2020-01',
    type: '行业TOP5平均',
    value: 4.2,
    color: '#6195FF'
  }, {
    date: '2020-02',
    type: '行业TOP5平均',
    value: 5.4,
    color: '#6195FF'
  }, {
    date: '2020-03',
    type: '行业TOP5平均',
    value: 3.7,
    color: '#6195FF'
  }, {
    date: '2019-10',
    type: '中收占比',
    value: 0.05,
    color: '#F9BB4C'
  }, {
    date: '2019-11',
    type: '中收占比',
    value: 0.08,
    color: '#F9BB4C'
  }, {
    date: '2019-12',
    type: '中收占比',
    value: 0.10,
    color: '#F9BB4C'
  }, {
    date: '2020-01',
    type: '中收占比',
    value: 0.00,
    color: '#F9BB4C'
  }, {
    date: '2020-02',
    type: '中收占比',
    value: 0.30,
    color: '#F9BB4C'
  }, {
    date: '2020-03',
    type: '中收占比',
    value: 0.20,
    color: '#F9BB4C'
  }, {
    date: '2019-10',
    type: '行业平均',
    value: 4.1,
    color: '#8759E5'
  }, {
    date: '2019-11',
    type: '行业平均',
    value: 3.8,
    color: '#8759E5'
  }, {
    date: '2019-12',
    type: '行业平均',
    value: 2.0,
    color: '#8759E5'
  }, {
    date: '2020-01',
    type: '行业平均',
    value: 3.8,
    color: '#8759E5'
  }, {
    date: '2020-02',
    type: '行业平均',
    value: 2.5,
    color: '#8759E5'
  }, {
    date: '2020-03',
    type: '行业平均',
    value: 1.5,
    color: '#8759E5'
  }],
  data8: [{
    date: '2019-12',
    type: '总体',
    value: 38,
    color: '#8759E5'
  },
  {
    date: '2020-01',
    type: '总体',
    value: 28,
    color: '#8759E5'
  },
  {
    date: '2020-02',
    type: '总体',
    value: 35,
    color: '#8759E5'
  },
  {
    date: '2020-03',
    type: '总体',
    value: 39,
    color: '#8759E5'
  },
  {
    date: '2020-04',
    type: '总体',
    value: 37,
    color: '#8759E5'
  },
  {
    date: '2020-05',
    type: '总体',
    color: '#8759E5'
  },
  {
    date: '2020-06',
    type: '总体',
    color: '#8759E5'
  },

  {
    date: '2019-12',
    type: '授信客户',
    value: 28,
    color: '#8759E5'
  },
  {
    date: '2020-01',
    type: '授信客户',
    value: 18,
    color: '#8759E5'
  },
  {
    date: '2020-02',
    type: '授信客户',
    value: 25,
    color: '#8759E5'
  },
  {
    date: '2020-03',
    type: '授信客户',
    value: 29,
    color: '#8759E5'
  },
  {
    date: '2020-04',
    type: '授信客户',
    value: 27,
    color: '#8759E5'
  },
  {
    date: '2020-05',
    type: '授信客户',
    color: '#8759E5'
  },
  {
    date: '2020-06',
    type: '授信客户',
    color: '#8759E5'
  },
  {
    date: '2019-12',
    type: '非授信',
    value: 18,
    color: '#8759E5'
  },
  {
    date: '2020-01',
    type: '非授信',
    value: 8,
    color: '#8759E5'
  },
  {
    date: '2020-02',
    type: '非授信',
    value: 15,
    color: '#8759E5'
  },
  {
    date: '2020-03',
    type: '非授信',
    value: 19,
    color: '#8759E5'
  },
  {
    date: '2020-04',
    type: '非授信',
    value: 17,
    color: '#8759E5'
  },
  {
    date: '2020-05',
    type: '非授信',
    color: '#8759E5'
  },
  {
    date: '2020-06',
    type: '非授信',
    color: '#8759E5'
  }

  ]
}

// 123
