import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import jq from 'jquery'
import '../static/css/normalize.css'

require('./utils/antvF2')

Vue.config.productionTip = false
Vue.prototype.$ = jq
// Vue.prototype.transitionName = 'slide-right'
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
