const analysis = [{
  path: '/analysis/loan',
  name: 'analysisLoan',
  component: () => import('../views/analysis/loan/index.vue'),
  meta: {
    title: '招商局集团有限公司'
  }
},
{
  path: '/analysis/complexity',
  name: 'analysis-complexity',
  component: () => import('../views/analysis/complexity/index.vue'),
  meta: {
    title: '招商局集团有限公司'
  }
},
{
  path: '/analysis/overview',
  name: 'analysis-overview',
  component: () => import('../views/analysis/overview/index.vue'),
  meta: {
    title: '招商局集团有限公司'
  }
},
{
  path: '/analysis/risk',
  name: 'analysis-risk',
  component: () => import('../views/analysis/risk/index.vue'),
  meta: {
    title: '招商局集团有限公司'
  }
},
{
  path: '/analysis/intermediateIncome',
  name: 'analysis-intermediateIncome',
  component: () => import('../views/analysis/intermediateIncome/index.vue'),
  meta: {
    title: '招商局集团有限公司'
  }
}
]

export default analysis
