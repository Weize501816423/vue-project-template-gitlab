const customer = [{
  path: '/customer/Bonus',
  name: 'Bonus',
  component: () => import('../views/customer/bonus/Home.vue'),
  meta: {
    title: '客户经理看板'
  }
}, {
  path: '/customer/Deposit',
  name: 'Deposit',
  component: () => import('../views/customer/deposit/Home.vue'),
  meta: {
    title: '客户经理看板'
  }
},
{
  path: '/customer/Loan',
  name: 'Loan',
  component: () => import('../views/customer/loan/Home.vue'),
  meta: {
    title: '客户经理看板'
  }
},
{
  path: '/customer/IntermediateIncome',
  name: 'IntermediateIncome',
  component: () => import('../views/customer/intermediateIncome/Home.vue'),
  meta: {
    title: '客户经理看板'
  }
}
]

export default customer
