import Vue from 'vue'
import VueRouter from 'vue-router'
import customer from './customer'
import analysis from './analysis'

Vue.use(VueRouter)

const routes = [{
  path: '/',
  name: 'Home',
  component: () => import('../views/Home')
}, {
  path: '/test',
  name: 'test',
  component: () => import('../views/test.vue'),
  meta: {
    title: '测试页面'
  }
}, {
  path: '/customer',
  name: 'customer',
  component: () => import('../views/customer/home'),
  meta: {
    title: '客户经理看板'
  },
  children: [
    ...customer
  ]
}, {
  path: '/analysis',
  name: 'analysis',
  component: () => import('../views/analysis/home'),
  meta: {
    title: '客户综合价值分析'
  },
  children: [
    ...analysis
  ]
}, {
  path: '/analysis/search',
  name: 'analysisSearch',
  component: () => import('../views/analysis/search'),
  meta: {
    title: '招商局集团有限公司'
  }
}
]

const router = new VueRouter({
  routes
})

router.afterEach((to, from, next) => {
  window.scrollTo(0, 0)
})

export default router
