import Vue from 'vue'

const F2 = require('@antv/f2/lib/core')
require('@antv/f2/lib/geom/interval')
require('@antv/f2/lib/geom/line')
require('@antv/f2/lib/scale/time-cat')
require('@antv/f2/lib/geom/interval')
require('@antv/f2/lib/coord/polar')
require('@antv/f2/lib/geom/adjust/')
require('@antv/f2/lib/geom/area')

// 加载点图
require('@antv/f2/lib/component/guide/point')
require('@antv/f2/lib/component/guide/text')
require('@antv/f2/lib/component/guide/line')
require('@antv/f2/lib/component/guide/html')
const Guide = require('@antv/f2/lib/plugin/guide')

const PieLabel = require('@antv/f2/lib/plugin/pie-label') // 引入 PieLabel 模块

// （插件）图例
const Legend = require('@antv/f2/lib/plugin/legend')

// 注册提示信息模块
const Tooltip = require('@antv/f2/lib/plugin/tooltip')

const IntervalLabel = require('@antv/f2/lib/plugin/interval-label')

const Animation = require('@antv/f2/lib/animation/detail')
F2.Chart.plugins.register([Tooltip, Guide, PieLabel, IntervalLabel, Legend, Animation])

Vue.prototype.$F2 = F2
